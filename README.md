We are living in a time where custom emojis are a thing and are reused in more and more apps. I manage my own ones with this project.

## Emoji previews

| Name | Preview |
| --- | --- |
| `dancing-chicken` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/dancing-chicken.gif" width="28" alt="dancing-chicken"/> |
| `dancing-mario-luigi` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/dancing-mario-luigi.gif" width="28" alt="dancing-mario-luigi"/> |
| `pompom` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/pompom.gif" width="28" alt="pompom"/> |
| `fine` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/fine.gif" width="28" alt="fine"/> |
| `plus-two` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/plus-two.png" width="28" alt="plus-two"/> |
| `slack` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/slack.png" width="28" alt="slack"/> |
| `vscode` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/vscode.png" width="28" alt="sketch"/> |
| `gitlab` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/gitlab.png" width="28" alt="gitlab"/> |
| `github` | <img src="https://gitlab.com/andreaskaemmerle/my-emojis/raw/master/src/github.png" width="28" alt="github"/> |

## References

* [Emojipedia](https://emojipedia.org)
* [Slackmojis](https://slackmojis.com)

## Contributing

1. Create an [issue](https://gitlab.com/andreaskaemmerle/my-emojis/issues/new) or [merge request](https://gitlab.com/andreaskaemmerle/my-emojis/merge_requests/new)
2. Assign it to @andreaskaemmerle
